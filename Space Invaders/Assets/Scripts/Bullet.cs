using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private const float BULLET_SPEED = GameSettings.BULLET_SPEED;
    public Collider2D collider2D;
    public AudioSource audioSource;
    public GameObject sprite;
    public float moveDirection;
    private bool canMove = true;
    public string opponentTag;
    public string opponentShotTag;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            transform.Translate(Vector2.up * moveDirection * Time.deltaTime * BULLET_SPEED);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(opponentTag) || 
            collision.gameObject.CompareTag("Shield") ||
            collision.gameObject.CompareTag(opponentShotTag) ||
            collision.gameObject.CompareTag("ExtraEnemy"))
        {
            /* Destroy the sprite and the collider, disable movement, start the audio, and after the audio is done playing,
               destroy the whole object.*/

            canMove = false;

            audioSource.PlayOneShot(audioSource.clip);

            Destroy(sprite);
            Destroy(collider2D);

            Destroy(gameObject, audioSource.clip.length);
        }
        else if (collision.gameObject.CompareTag("Boundary"))
        {
            // Destroy the object straight away if it hits the boundary without playing the audio.
            Destroy(gameObject);
        }
    }
}
