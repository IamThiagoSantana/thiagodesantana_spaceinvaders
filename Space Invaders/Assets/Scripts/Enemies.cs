using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class EnemiesMovement : MonoBehaviour
{
    private bool movingRight = true;
    private bool movingVertically = false;
    public GameObject bullet;
    private Vector2 horizontalMoveDistance = new Vector2(0.9f,0);
    private float verticalMoveDistance = 0.3f;
    private float moveTimer = 0.5f;
    private const float MOVING_TIME = 0.03f;
    private const float MIN_MOVE_SPEED = 1.2f;
    private const float MAX_ENEMY_RIGHT = GameSettings.MAX_PLAYER_RIGHT;
    private const float MAX_ENEMY_LEFT = GameSettings.MAX_PLAYER_LEFT;
    // Start is called before the first frame update
    void Start()
    {

        InvokeRepeating(nameof(Shoot), 1.5f, 1.5f);
    }
    private void Shoot()
    {
        GameObject[] aliens = GameObject.FindGameObjectsWithTag("Enemy");
        int randomAlienIndex = Random.Range(0, aliens.Length);

        GameObject randomAlien = aliens[randomAlienIndex];

        Instantiate(bullet, randomAlien.transform.position, Quaternion.identity);
    }
    private float GetExtraDistance(float alienX, float boundaryX)
    {
        return alienX - boundaryX;
    }
    private bool IsOnBarrier()
    {
        GameObject[] aliens = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < aliens.Length; i++)
        {
            GameObject alien = aliens[i];

            if (alien.transform.position.x >= MAX_ENEMY_RIGHT)
            {
                float extraDistance = GetExtraDistance(alien.transform.position.x, MAX_ENEMY_RIGHT);
                transform.position = new Vector2(transform.position.x - extraDistance, transform.position.y);
                return true;
            }
            else if (alien.transform.position.x <= MAX_ENEMY_LEFT)
            {
                float extraDistance = GetExtraDistance(alien.transform.position.x, MAX_ENEMY_LEFT);
                transform.position = new Vector2(transform.position.x - extraDistance, transform.position.y);
                return true;
            }
        }
        return false;
    }
    private bool IsAlienTooClose()
    {
        GameObject[] aliens = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < aliens.Length; i++)
        {
            GameObject alien = aliens[i];

            if (alien.transform.position.y <= GameSettings.ENEMY_HEIGHT_LIMIT)
            {
                return true;
            }
        }

        return false;
    }
    private void MoveEnemies()
    {
        
        if (movingVertically)
        {
            transform.position -= new Vector3(0, verticalMoveDistance, 0);
            movingRight = !movingRight;
            movingVertically = false;

            if (IsAlienTooClose())
            {
                Game.getInstance().GameOver();
            }
        }

        else if (movingRight)
        {
            transform.Translate(horizontalMoveDistance);
            if (IsOnBarrier())
            {
                movingVertically = true;
            }
        }

        else
        {
            transform.Translate(-horizontalMoveDistance);
            if (IsOnBarrier())
            {
                movingVertically = true;
            }
        }

        moveTimer = GetMovingSpeed();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
        {
            Destroy(gameObject);
            Game.getInstance().EndRound();
        }

        if (moveTimer < 0)
        {
            MoveEnemies();
        }
        moveTimer -= Time.deltaTime;
    }

    private float GetMovingSpeed()
    {
        GameObject[] aliens = GameObject.FindGameObjectsWithTag("Enemy");
        return Math.Max(aliens.Length * MOVING_TIME, MIN_MOVE_SPEED);
    }
}
