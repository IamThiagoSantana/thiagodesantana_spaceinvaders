using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDataAdder : MonoBehaviour
{
    public TMP_InputField playerNameText;

    public void HandleAdd()
    {
        string playerName = playerNameText.text.Replace(",", "");
        playerName = playerName.Replace("\n", "");

        if (playerName.Length > 0)
        {
            PlayerPrefs.SetString("SessionName", playerName);

            Leaderboard.getInstance().AddEntry();
        }

        SceneManager.LoadScene("MenuScene");
    }
}
