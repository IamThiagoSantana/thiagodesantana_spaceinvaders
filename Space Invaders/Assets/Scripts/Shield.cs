using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private int life = 20;
    private SpriteRenderer renderer;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerShot") || collision.gameObject.CompareTag("EnemyShot"))
        {
            life--;
            if (life <= 15 && life > 10)
            {
                renderer.sprite = sprite1;
            }
            else if (life <= 10 && life > 5)
            {
                renderer.sprite = sprite2;
            }
            else if (life <= 5 && life > 0)
            {
                renderer.sprite = sprite3;
            }
            else if (life == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
