using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;

public static class GameSettings
{
    public const float MAX_PLAYER_LEFT = -15.53f;
    public const float MAX_PLAYER_RIGHT = 8.77f;
    public const float BULLET_SPEED = 5f;
    public const float ENEMY_HEIGHT_LIMIT = -5.31f;
}
