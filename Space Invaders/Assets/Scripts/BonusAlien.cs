using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusAlien : Enemy
{
    private float horizontalSpeed = 3f;
    private int[] possibleScores = new int[] { 150, 200, 250 };
    private float deathAnimationLength;
    private float extraAlienEndX = 10.41f;

    // Start is called before the first frame update
    void Start()
    {
        deathAnimationLength = FindAnimationLength("ExtraAlienDeath");
        int randomIndex = Random.Range(0, possibleScores.Length);
        score = possibleScores[randomIndex];
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(horizontalSpeed * Time.deltaTime, 0f, 0f);

        if (transform.position.x >= extraAlienEndX)
        {
            Destroy(gameObject);
        }
    }
}
