using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    private static Leaderboard _instance;
    private string filePath; 
   
    private string[] topFive;
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    public static Leaderboard getInstance()
    {
        return _instance;
    }
    // Start is called before the first frame update
    void Start()
    {
        filePath = Application.persistentDataPath + "/leaderboard.txt";

        if (!File.Exists(filePath))
        {
            File.Create(filePath).Close();
        }
    }
    public string GetEntryFromTopFive(int i)
    {
        if (topFive == null)
        {
            topFive = GetTopFive();
        }

        if (topFive.Count() > i)
        {
            return topFive[i];
        }

        return $"{i}.";
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private string[] GetTopFive()
    {
        string[] topFive = new string[5];

        string[] lines = File.ReadAllLines(filePath);
        Dictionary<string, int> playerScores = new Dictionary<string, int>();
        foreach (string line in lines)
        {
            string[] parts = line.Split(',');
            if (parts.Length == 2)
            {
                string playerName = parts[0];
                int playerScore = int.Parse(parts[1]);

                playerScores[playerName] = playerScore;
            }
        }

        List<KeyValuePair<string, int>> sortedPlayerScores = new List<KeyValuePair<string, int>>(playerScores);

        sortedPlayerScores.Sort((x, y) => y.Value.CompareTo(x.Value));

        int entriesToShow = Mathf.Min(5, sortedPlayerScores.Count);
        for (int i = 0; i < entriesToShow; i++)
        {
            topFive[i] = $"{i+1}. {sortedPlayerScores[i].Key}: {sortedPlayerScores[i].Value}\n";
        }

        return topFive;
    }
    public void AddEntry()
    {
        string playerName = PlayerPrefs.GetString("SessionName");
        int playerScore = PlayerPrefs.GetInt("SessionScore");
        bool playerScoreUpdated = false;

        string[] lines = File.ReadAllLines(filePath);
        StringBuilder updatedContent = new StringBuilder();

        foreach (string line in lines)
        {

            string[] parts = line.Split(',');
            if (parts.Length == 2)
            {
                string existingPlayerName = parts[0];
                int existingPlayerScore = int.Parse(parts[1]);


                if (existingPlayerName == playerName)
                {

                    existingPlayerScore = playerScore;
                    playerScoreUpdated = true;
                }


                updatedContent.AppendLine($"{existingPlayerName},{existingPlayerScore}");
            }
        }


        if (!playerScoreUpdated)
        {
            updatedContent.AppendLine($"{playerName},{playerScore}");
        }


        File.WriteAllText(filePath, updatedContent.ToString());

        topFive = GetTopFive();
    }
}
