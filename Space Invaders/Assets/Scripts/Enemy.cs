using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator animator;
    public Collider2D collider2D;
    public int score;
    private float deathAnimationLength;
    // Start is called before the first frame update
    void Start()
    {
        deathAnimationLength = FindAnimationLength("Alien_Death");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected float FindAnimationLength(string animationName)
    {
        RuntimeAnimatorController animationController = animator.runtimeAnimatorController;

        foreach (AnimationClip clip in animationController.animationClips)
        {
            if (clip.name.Equals(animationName))
            {
                return clip.length;
            }
        }

        return 0;
    }
    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerShot"))
        {
            Destroy(collider2D);
            animator.SetBool("IsDead", true);
            Destroy(gameObject, deathAnimationLength);
            Game.getInstance().AddScore(score);
        }
    }
}
