using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    private static Game _instance;
    private int _currentScore = 0;
    private int _currentLife = 3;
    private int scoreCheckpoint = 0;
    public GameObject aliens;
    public GameObject extraAlien;
    private int _round = 1;
    private bool running = false;
    private bool isPaused = false;
    private float verticalAlienMove = 0.3f;
    private Vector3 extraAlienStartPos = new Vector3(-17.09f, 2.92f, -0.894f);
    private const float LOWEST_START_POSITION = 1.5f;
    public int currentScore
    {
        get => _currentScore;
    }
    public int currentLife
    {
        get => _currentLife;
    }
    public int round
    {
        get => _round;
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        
    }

    public static Game getInstance()
    {
        return _instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        _currentScore = 0;
        _currentLife = 3;
        _round = 1;
        running = true;
        StartRound();
        StartCoroutine(BonusAlienSpawner());
    }

    IEnumerator BonusAlienSpawner()
    {
        while(running)
        {
            if (!isPaused)
            {
                yield return new WaitForSeconds(15);
                Instantiate(extraAlien, extraAlienStartPos, Quaternion.identity);
            }
        }
    }
    public void AddScore(int score)
    {
        _currentScore += score;

        if ((_currentScore - scoreCheckpoint) / 1000 >= 1)
        {
            scoreCheckpoint += 1000;
            AddLife();
        }
    }

    public void LoseLife()
    {
        _currentLife--;
        if (_currentLife == 0)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        running = false;
        isPaused = true;
        PlayerPrefs.SetInt("SessionScore", _currentScore);
        PlayerPrefs.Save();
        SceneManager.LoadScene("NameChoosingScene");
    }

    private void AddLife()
    {
        if (_currentLife < 6)
        {
            _currentLife++;
        }
    }
    
    public void EndRound()
    {
        isPaused = true;
        Thread.Sleep(2000);
        _round++;
        StartRound();
    }

    private void StartRound()
    {
        isPaused = false;
        float spawnPosition = Mathf.Min(_round * verticalAlienMove, LOWEST_START_POSITION);
        Instantiate(aliens, Vector3.zero - new Vector3(0, spawnPosition), Quaternion.identity);
    }
}
