using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public float moveSpeed;
    public GameObject bullet;
    private const float MAX_RIGHT = GameSettings.MAX_PLAYER_RIGHT;
    private const float MAX_LEFT = GameSettings.MAX_PLAYER_LEFT;
    private bool canShoot = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void MovePlayer(int moveDirection)
    {
        
        transform.Translate(new Vector2(moveSpeed * moveDirection * Time.deltaTime, 0));
    }

    void HandlePlayerMovement()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.position.x >= MAX_RIGHT)
            {
                transform.position = new Vector2(MAX_RIGHT, transform.position.y);
                return;
            }
            MovePlayer(1);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            if (transform.position.x <= MAX_LEFT)
            {
                transform.position = new Vector2(MAX_LEFT, transform.position.y);
                return;
            }
            MovePlayer(-1);
        }
    }

    IEnumerator SpawnBullet()
    {
        canShoot = false;
        Instantiate(bullet, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.5f);
        canShoot = true;
    }
    void HandlePlayerShoot()
    {
        if (Input.GetKey(KeyCode.Space) && canShoot)
        {
            StartCoroutine(SpawnBullet());
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyShot"))
        {
            Game.getInstance().LoseLife();
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandlePlayerMovement();
        HandlePlayerShoot();
       
    }
}
