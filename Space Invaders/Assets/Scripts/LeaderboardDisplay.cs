using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderboardDisplay : MonoBehaviour
{
    public TextMeshProUGUI top1;
    public TextMeshProUGUI top2;
    public TextMeshProUGUI top3;
    public TextMeshProUGUI top4;
    public TextMeshProUGUI top5;
    private Leaderboard leaderboard = Leaderboard.getInstance();
    // Start is called before the first frame update
    void Start()
    {

        top1.text = leaderboard.GetEntryFromTopFive(0);
        top2.text = leaderboard.GetEntryFromTopFive(1);
        top3.text = leaderboard.GetEntryFromTopFive(2);
        top4.text = leaderboard.GetEntryFromTopFive(3);
        top5.text = leaderboard.GetEntryFromTopFive(4);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
